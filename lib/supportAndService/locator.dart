import 'package:assignment/supportAndService/mydatastore.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton<DataStore>(() => DataStore());
}
