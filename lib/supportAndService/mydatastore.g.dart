// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mydatastore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$DataStore on MyDataStore, Store {
  late final _$dataAtom = Atom(name: 'MyDataStore.data', context: context);

  @override
  ObservableList<MoviesModel> get data {
    _$dataAtom.reportRead();
    return super.data;
  }

  @override
  set data(ObservableList<MoviesModel> value) {
    _$dataAtom.reportWrite(value, super.data, () {
      super.data = value;
    });
  }

  late final _$MyDataStoreActionController =
      ActionController(name: 'MyDataStore', context: context);

  @override
  void addData(MoviesModel moviesModel) {
    final _$actionInfo =
        _$MyDataStoreActionController.startAction(name: 'MyDataStore.addData');
    try {
      return super.addData(moviesModel);
    } finally {
      _$MyDataStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateData(int index, MoviesModel moviesModel) {
    final _$actionInfo = _$MyDataStoreActionController.startAction(
        name: 'MyDataStore.updateData');
    try {
      return super.updateData(index, moviesModel);
    } finally {
      _$MyDataStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteData(int index) {
    final _$actionInfo = _$MyDataStoreActionController.startAction(
        name: 'MyDataStore.deleteData');
    try {
      return super.deleteData(index);
    } finally {
      _$MyDataStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
data: ${data}
    ''';
  }
}
