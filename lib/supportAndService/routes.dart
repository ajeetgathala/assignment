
import 'package:assignment/widgets/EditAddPage.dart';
import 'package:assignment/widgets/HomePage.dart';
import 'package:auto_route/annotations.dart';

@MaterialAutoRouter(routes: <AutoRoute>[

  AutoRoute(
    page: HomePage,
    initial: true
  ),
  AutoRoute(
      page: EditAddPage,
      path: "/create-movie/:id"
  ),
])
class $AppRouter {}