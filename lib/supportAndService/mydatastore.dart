import 'package:assignment/models/MoviesModel.dart';
import 'package:mobx/mobx.dart';

part 'mydatastore.g.dart';

class DataStore = MyDataStore with _$DataStore;

abstract class MyDataStore with Store{
  @observable
  ObservableList<MoviesModel> data =  ObservableList<MoviesModel>.of([
    MoviesModel(
         "1659073011",
         "Power Ranger",
         "Dean Israelite",
         "Power Ranger Movie Directed by Dean Israelite",
         "Action,Sci-Fi"),
    MoviesModel(
         "1659073036",
         "Frozen",
         "Chris Buck",
         "Frozen Movie directed by Chris Buck",
         "Animation")
  ]);


  @action
  void addData(MoviesModel moviesModel){
    data.add(moviesModel);
  }

  @action
  void updateData(int index, MoviesModel moviesModel){
    data[index]=moviesModel;
  }

  @action
  void deleteData(int index){
    data.removeAt(index);
  }
}
