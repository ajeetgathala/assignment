class MoviesType {
  final int id;
  final String name;
  bool selected;

  MoviesType({required this.id, required this.name, required this.selected});
}