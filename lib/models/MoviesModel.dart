import 'package:mobx/mobx.dart';

@observable
class MoviesModel {
  @observable
  String id;
  @observable
  String title;
  @observable
  String director;
  @observable
  String summary;
  @observable
  String genres;

  MoviesModel(this.id, this.title, this.director, this.summary, this.genres);
}
