import 'package:assignment/supportAndService/locator.dart';
import 'package:assignment/widgets/EditAddPage.dart';
import 'package:assignment/models/MoviesModel.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../supportAndService/mydatastore.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<MoviesModel> dataa = [];
  List<MoviesModel> searchData = [];

  // StackRouter? stackRouter;
  String searchText = '';

  @override
  void initState() {
    dataa = getIt.get<DataStore>().data;
    super.initState();
    // stackRouter = context.router;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Movies Collection"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // stackRouter?.push(EditAddPage));

          bool abc = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EditAddPage(false, null, null)),
          );
          if (abc) {
            setState(() {
              dataa = getIt.get<DataStore>().data;
            });
          }
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              child: TextField(
                onChanged: (text) {
                  searchText = text;
                  search();
                },
                decoration: InputDecoration(
                  hintText: 'Search...',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            Expanded(
                child: searchText.isEmpty
                    ? Container(
                        margin: EdgeInsets.only(top: 15, left: 20, right: 20),
                        child: Observer(
                          builder: (context) {
                            return ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: dataa.length,
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return listItem(dataa, index);
                                });
                          },
                        ))
                    : Container(
                        margin: EdgeInsets.only(top: 15, left: 20, right: 20),
                        child: Observer(
                          builder: (context) {
                            return ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: searchData.length,
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return listItem(searchData, index);
                                });
                          },
                        ))),
          ],
        ),
      ),
    );
  }

  Widget listItem(List<MoviesModel> data, int index) {
    return InkWell(
        onTap: () {
          setState(() async {
            bool abc = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => EditAddPage(true, index, data[index])),
            );
            if (abc) {
              setState(() {
                dataa = getIt.get<DataStore>().data;
              });
            }
          });
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          padding: EdgeInsets.all(5.0),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black54, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  bottom: 10.0,
                ),
                alignment: Alignment.centerLeft,
                child: Text(
                  data[index].title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  bottom: 8.0,
                ),
                alignment: Alignment.centerLeft,
                child: Text(
                  data[index].director,
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Text(
                  data[index].genres,
                  style: TextStyle(color: Colors.black87, fontSize: 15.0),
                ),
              )
            ],
          ),
        ));
  }

  void search() {
    if (searchText.trim() == '') {
      searchData.clear();
      setState(() {
        dataa = getIt.get<DataStore>().data;
      });
    } else {
      searchData.clear();
      for (int i = 0; i < dataa.length; i++) {
        if (dataa[i].title.toUpperCase().contains(searchText.toUpperCase())) {
          setState(() {
            searchData.add(dataa[i]);
          });
        }
      }
    }
    setState(() {});
  }
}
