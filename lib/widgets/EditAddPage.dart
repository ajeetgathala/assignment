import 'package:assignment/models/MoviesModel.dart';
import 'package:assignment/supportAndService/locator.dart';
import 'package:flutter/material.dart';

import '../models/MoviesType.dart';
import '../supportAndService/mydatastore.dart';

class EditAddPage extends StatefulWidget {
  bool needUpdate;
  int? position;
  MoviesModel? moviesModel;

  EditAddPage(this.needUpdate, this.position, this.moviesModel);

  @override
  State<EditAddPage> createState() => _EditAddPageState();
}

class _EditAddPageState extends State<EditAddPage> {
  static List<MoviesType> moviesType = [];
  static List<String> selectedMoviesType = [];

  TextEditingController controllerTitle = TextEditingController();
  TextEditingController controllerDirector = TextEditingController();
  TextEditingController controllerSummary = TextEditingController();

  late MoviesModel moviesModel = MoviesModel("", "", "", "", "");

  @override
  initState() {
    super.initState();
    setTypes();
    if (widget.needUpdate) {
      setUpdateData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: <Widget>[
          if (widget.needUpdate)
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                deleteData();
              },
              child: Text('Delete'),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              if (widget.needUpdate) {
                updateData();
              } else {
                saveData();
              }
            },
            child: Text(widget.needUpdate ? 'Update' : 'Save'),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 30.0, bottom: 5.0),
                  child: TextField(
                    onChanged: (text) {
                      moviesModel.title = text;
                    },
                    controller: controllerTitle,
                    decoration: InputDecoration(
                      hintText: 'Title',
                      labelText: 'Title',
                      labelStyle: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.w500),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  )),
              Container(
                  margin: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 30.0, bottom: 5.0),
                  child: TextField(
                    onChanged: (text) {
                      moviesModel.director = text;
                    },
                    controller: controllerDirector,
                    decoration: InputDecoration(
                      hintText: 'Director',
                      labelText: 'Director',
                      labelStyle: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.w500),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  )),
              Container(
                  margin: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 30.0, bottom: 5.0),
                  child: TextField(
                    onChanged: (text) {
                      moviesModel.summary = text;
                    },
                    controller: controllerSummary,
                    minLines: 5,
                    maxLines: 6,
                    decoration: InputDecoration(
                      alignLabelWithHint: true,
                      hintText: 'Summary',
                      labelText: 'Summary',
                      labelStyle: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.w500),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  )),
              Container(
                  margin: EdgeInsets.only(top: 15, left: 20, right: 20),
                  child: GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 4,
                      shrinkWrap: true,
                      children: List.generate(moviesType.length, (index) {
                        return listItem(index);
                      }))),
              // Container(
              //       margin: EdgeInsets.only(top: 15, left: 20, right: 20),
              //       child: ListView.builder(
              //           shrinkWrap: true,
              //           scrollDirection: Axis.horizontal,
              //           itemCount: 4,
              //           physics: const NeverScrollableScrollPhysics(),
              //           itemBuilder: (context, index) {
              //             return listItem(index);
              //           }),
              //     ),
            ],
          ),
        ),
      ),
    );
  }

  Widget listItem(int index) {
    return Container(
        alignment: index == 3 ? Alignment.centerLeft : Alignment.center,
        child: InkWell(
            onTap: () {
              if (moviesType[index].selected) {
                setState(() {
                  selectedMoviesType.remove(moviesType[index].name);
                  moviesType[index].selected = false;
                });
              } else {
                setState(() {
                  selectedMoviesType.add(moviesType[index].name);
                  moviesType[index].selected = true;
                });
              }
              moviesModel.genres = selectedMoviesType.join(',');
            },
            child: Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: moviesType[index].selected
                          ? Colors.blue
                          : Colors.black,
                      width: moviesType[index].selected ? 2.0 : 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Text(
                moviesType[index].name,
                style: TextStyle(
                    color:
                        moviesType[index].selected ? Colors.blue : Colors.black,
                    fontWeight: moviesType[index].selected
                        ? FontWeight.w500
                        : FontWeight.w400),
              ),
            )));
  }

  void saveData() {
    moviesModel.id = DateTime.now().millisecondsSinceEpoch.toString();
    if (moviesModel.title.trim() == "") {
      showToast(context, 'Please Enter Title');
    } else if (moviesModel.director.trim() == "") {
      showToast(context, 'Please Enter Director Name');
    } else if (moviesModel.summary.trim() == "") {
      showToast(context, 'Please Enter Movie Summary');
    } else if (moviesModel.genres.trim() == "") {
      showToast(context, 'Please Select Movie Categories');
    } else {
      selectedMoviesType.clear();
      getIt.get<DataStore>().addData(moviesModel);
      Navigator.pop(context, true);
    }
  }

  void showToast(BuildContext context, String message) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(message),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void setUpdateData() {
    moviesModel = widget.moviesModel!;
    controllerTitle.text = moviesModel.title;
    controllerDirector.text = moviesModel.director;
    controllerSummary.text = moviesModel.summary;
    selectedMoviesType = moviesModel.genres.split(',');
    for (int i = 0; i < moviesType.length; i++) {
      bool selected = false;
      for (int j = 0; j < selectedMoviesType.length; j++) {
        if (moviesType[i].name == selectedMoviesType[j]) {
          selected = true;
          break;
        }
      }
      if (selected) {
        setState(() {
          moviesType[i].selected = true;
        });
      }
    }
  }

  void updateData() {
    if (moviesModel.title.trim() == "") {
      showToast(context, 'Please Enter Title');
    } else if (moviesModel.director.trim() == "") {
      showToast(context, 'Please Enter Director Name');
    } else if (moviesModel.summary.trim() == "") {
      showToast(context, 'Please Enter Movie Summary');
    } else if (moviesModel.genres.trim() == "") {
      showToast(context, 'Please Select Movie Categories');
    } else {
      setState(() {
        selectedMoviesType.clear();
        getIt.get<DataStore>().updateData(widget.position!, moviesModel);
        Navigator.pop(context, true);
      });
    }
  }

  void setTypes() {
    moviesType = [
      MoviesType(id: 1, name: "Action", selected: false),
      MoviesType(id: 2, name: "Animation", selected: false),
      MoviesType(id: 3, name: "Drama", selected: false),
      MoviesType(id: 4, name: "Sci-Fi", selected: false),
    ];
  }

  void deleteData() {
    selectedMoviesType.clear();
    getIt.get<DataStore>().deleteData(widget.position!);
    Navigator.pop(context, true);
  }
}
